import * as React from 'react';
import { CommandBar, ICommandBarItemProps } from '@fluentui/react/lib/CommandBar';
import { initializeIcons } from '@fluentui/font-icons-mdl2';
import {connect} from 'react-redux'
import { useHistory } from 'react-router-dom';
import {Separator, Toggle} from "@fluentui/react";
import {getAPI, getTheme} from "../../Core/Global/global.selectors";
import {setTheme} from "../../Core/Global/global.actions";
import {useEffect, useState} from "react";
import {useCookies} from "react-cookie";



function Header(props){

    const[cookies, setCookie, removeCookie] = useCookies(["AccountCookie"]);
    const[authCookies] = useCookies(["GoogleAccountcookie"]);
    const[name, setName] = useState("");
    useEffect(() => {
        if(cookies.AccountCookie !== undefined && props.api !== undefined){
            let token = cookies.AccountCookie;
            props.api.get("account/info/"+ token).then(res => {
                setName(res.data.username)})
                .catch(err => {console.log(err)})
        }
        if(authCookies.GoogleAccountcookie !== undefined){
            setName([authCookies.GoogleAccountcookie.username]);
        }
    },[props.api]);

    function toggleTheme(){
        let newTheme;
        let oldTheme = props.theme;
        oldTheme ? newTheme = false : newTheme = true;
        props.dispatch(setTheme(newTheme));
    }
    const history = useHistory()
    const _items = [
        {
            key: 'Home',
            text: 'Home',
            iconProps: { iconName: 'Home' },
            onClick: () => history.push('/Home'),
        },
        {
            key: 'Food',
            text: 'Food',
            iconProps: { iconName: 'World' },
            onClick: () => history.push('/Food'),
        }

    ];

    const _overflowItems = [
        { key: 'move', text: 'Move to...', onClick: () => history.push('/Register'), iconProps: { iconName: 'MoveToFolder' } },
        { key: 'copy', text: 'Copy to...', onClick: () => console.log('Copy to'), iconProps: { iconName: 'Copy' } },
        { key: 'rename', text: 'Rename...', onClick: () => console.log('Rename'), iconProps: { iconName: 'Edit' } },
    ];

    const _farItems = [
        {
            text: name,
        },
        {
            key: 'Add Food',
            text: 'Add Food',
            ariaLabel: 'Change theme',
            iconOnly: true,
            iconProps: {iconName: 'Add'},
            onClick: () => history.push('/AddFood')
        },
        {
            key: 'darkMode',
            text: 'Change theme',
            ariaLabel: 'Change theme',
            iconOnly: true,
            iconProps: {iconName: 'Contrast'},
            onClick: toggleTheme
        },
        {
            key: 'Login',
            text: 'Login',
            // This needs an ariaLabel since it's icon-only
            ariaLabel: 'Login',
            iconOnly: true,
            iconProps: { iconName: 'AccountManagement' },
            onClick: () => history.push('/Register'),
        },

    ];
    initializeIcons()
    return (
        <div>
            <CommandBar
                items={_items}
                overflowItems={_overflowItems}
                farItems={_farItems}
                ariaLabel="Inbox actions"
                primaryGroupAriaLabel="Email actions"
                farItemsGroupAriaLabel="More actions"
            />
            <Separator className={"Separator"}/>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        theme : getTheme(state),
        api : getAPI(state)
    };
};
export default connect(mapStateToProps)(Header);