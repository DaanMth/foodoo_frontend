import {Route} from "react-router";
import {Row, Col} from "react-grid-system";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import React from "react";

function Home(props){

    return(
        <Route path={"/Home"} component={"/"}>
            <img style={{width: "100%"}} src={"Images/foodoofrontpage.png"}></img>
            <Row sm={2} md={4} lg={6}>
                <Card style={{width: "25%", marginTop:"20px", height: "250px", marginLeft: "100px"}}>
                    <CardActionArea>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {props.title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {props.text}
                            </Typography>
                        </CardContent>

                    </CardActionArea>
                </Card>
                <Card style={{width: "25%", marginTop:"20px", height: "250px", marginLeft: "150px"}}>
                    <CardActionArea>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {props.title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {props.text}
                            </Typography>
                        </CardContent>

                    </CardActionArea>
                </Card>
                <Card style={{width: "25%", marginTop:"20px", height: "250px", marginLeft: "170px"}}>
                    <CardActionArea>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {props.title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {props.text}
                            </Typography>
                        </CardContent>

                    </CardActionArea>
                </Card>

            </Row>
        </Route>
    );



}
export default Home

