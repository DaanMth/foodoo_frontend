import './TextFields.css';
import {useState} from "react";
import {PrimaryButton, TextField} from "@fluentui/react";
import {useHistory} from "react-router-dom";
import * as React from "react";
import {useCookies} from "react-cookie";
import {getAPI} from "../../../../../Core/Global/global.selectors";
import {connect} from "react-redux";
import GoogleLogin from 'react-google-login';

function TextFields(props){
    const[email, setEmail] = useState("");
    const[password, setPassword] = useState("");
    const[cookies, setCookie, removeCookie] = useCookies(["AccountCookie"]);
    const history = useHistory();

    function submit(){
        if(props.api !== undefined){
            props.api.post('/account/login', {
                email: email,
                password: password
            }).then(res => {
                setCookie("AccountCookie",res.data)
                history.push('Food')
            })
        }
    }

    function responseGoogle(response){
        if(props.api !== undefined){
            console.log(response.accessToken);
            console.log(response.tokenId);
            let token = response.tokenId;
            props.api.post('googleauth/' + token).then(res => {
                setCookie("GoogleAccountcookie", res.data)
            })
            history.push('Food')
        }
    }

    return(
        <div className={"textField"} style={{}}>
            <h1 className={"Title"}>Login!</h1>
            <TextField placeholder={"Email"} onChange={(e,value) => setEmail(value)}/>
            <TextField
                placeholder={"Password"}
                onChange={(e,value) => setPassword(value)}
                type="password"
                canRevealPassword
                revealPasswordAriaLabel="Show password"
            />
            <button className={"foodButton"}
                    style={{marginTop: "10px"}} onClick={submit}>Login</button>
            <div>Don't have an account yet? <a href={"http://localhost:3000/Register"}>Register now!</a></div>
            <div>
                <GoogleLogin
                    clientId="1084730342675-28r3kuko8c09bcn7sp367d98sm03tke2.apps.googleusercontent.com"
                    ButtonText="Login"
                    onSuccess={responseGoogle}
                    cookiePolicy={'single_host_origin'}
                />
            </div>
        </div>
    )
}
const mapStateToProps = (state) =>{
    return {
        api: getAPI(state)
    }
}
export default connect(mapStateToProps)(TextFields);