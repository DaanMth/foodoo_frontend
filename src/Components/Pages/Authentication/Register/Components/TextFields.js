import './TextFields.css';
import {useState} from "react";
import {PrimaryButton, TextField} from "@fluentui/react";
import {useHistory} from "react-router-dom";
import * as React from "react";
import {Col} from "react-grid-system";

function TextFields(){
    const[email, setEmail] = useState("");
    const[username, setUsername] = useState("");
    const[password, setPassword] = useState("");

    const history = useHistory();

    const axios = require('axios');
    const api = axios.create({
        baseURL:'http://localhost:5000/',
        timeout: 10000
    })

    function submit(){
        api.post('/account/register', {
            email: email,
            username: username,
            password: password
        }).then(res => { console.log(res.data) })
        history.push('Food')
    }

    return(
                <div className={"textField"} style={{}}>
                    <h1 className={"Title"}>Register now!</h1>
                    <TextField placeholder={"Email"} onChange={(e,value) => setEmail(value)}/>
                    <TextField placeholder={"Username"} onChange={(e,value) => setUsername(value)}/>
                    <TextField
                    placeholder={"Password"}
                    onChange={(e,value) => setPassword(value)}
                    type="password"
                    canRevealPassword
                    revealPasswordAriaLabel="Show password"
                />
                    <button className={"foodButton"}
                            style={{marginTop: "10px"}} onClick={submit}>Register</button>
                    <div>Already have an account? <a href={"http://localhost:3000/Login"}>Sign in!</a></div>
                </div>
    )
}
export default TextFields;