
import FoodCardList from "./FoodCardList";
import { useHistory } from "react-router-dom";
import {Separator} from "@fluentui/react";
import * as React from "react";



function Food(props) {

    const history = useHistory();

    return (
        <div>
        <FoodCardList style={{marginTop: '100px', marginBottom: "10px"}}/>
            <Separator className={"Separator2"}/>
        <button onClick={() => history.push("addfood")} className={"foodButton"} style={{marginTop: "10px"}}>Eten toevoegen</button>
        </div>
    );
}
export default Food