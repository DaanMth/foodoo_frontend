import {Row, Col} from "react-grid-system";
import {Separator} from "@fluentui/react";
import * as React from "react";
import {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {getAPI, getConnection, getTheme} from "../../../../Core/Global/global.selectors";
import {connect} from "react-redux";
import './Recipe.css';


function Recipe(props){

    const[recipe, setRecipe] = useState({});
    useEffect(()=>{
        getRecipe();
    },[recipe])

    const history = useHistory();
    const routeChange = () =>{
        let path = `Huts`;
        history.push(path);
    }

    function getRecipe(){
        if(props.api !== undefined){
            props.api.get('/recipe/' + props.id).then(res => {
                setRecipe(res.data);
            });
        }
    }

    function RemoveRecipe(){
        if(props.api !== undefined){
            props.api.post('/recipe/delete/' + props.id).then(() => {
                routeChange();
            });
        }
    }

    return <div>

        <div>
                <Row>
                    <Col sm={6} md={6} lg={6}>
                        <img className={"recipePicture"} style={{width: "700px", marginTop: "20px"}} src={"https://localhost:5001/Uploads/" + recipe.image} alt={"error"}/>
                    </Col>

                    <Col style={{marginTop: "20px"}} sm={6} md={6} lg={6}>
                        <Separator className={"Separator"}/>
                        <h1>Naam: <div style={{fontSize: "20px"}}>{recipe.name}</div></h1>
                        <h1>Ingredienten: <div style={{fontSize: "20px"}}> {recipe.ingredients}</div>
                        </h1>
                        <h1>Macro's:
                            <div style={{fontSize: "20px"}}>
                                <ul>
                                    <li>Kcal: {recipe.carbs}</li>
                                </ul>
                            </div>
                        </h1>
                        <h1>Voorbereiding: <div style={{fontSize: "12px"}}>

                            {recipe.preparation} </div></h1>

                    </Col>

                </Row>
            <Separator className={"Separator"} style={{margintop: "40px"}}/>
            <button className={"foodButton"}
                    style={{marginTop: "10px"}} onClick={RemoveRecipe}>Verwijder recept</button>
        </div>
    </div>
}
const mapStateToProps = (state) => {
    return {
        api: getAPI(state),
        connection: getConnection(state)
    };
};

export default connect(mapStateToProps)(Recipe);