import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {useHistory} from "react-router-dom";

function FoodCard(props){

    let history = useHistory()
    function redirectToRecipe(){
        console.log(props.id)
        history.push("/recipe/" + props.id)
    }

    return <div>
        <Card onClick={redirectToRecipe} style={{width: "100%", marginTop:"20px", height: "250px"}}>
            <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        height="140px"
                        image={props.image}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.text}
                        </Typography>
                    </CardContent>

            </CardActionArea>
        </Card>
    </div>
}
export default FoodCard;