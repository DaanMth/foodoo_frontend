import React, {useEffect, useState} from "react";
import FoodCard from "./FoodCard";
import {Row, Col} from "react-grid-system";

import { Link } from 'react-router-dom';
import axios from "axios";
import * as fs from "fs";
import {getAPI, getConnection} from "../../../Core/Global/global.selectors";
import {connect} from "react-redux";

function FoodCardList(props){
    const axios = require('axios');
    const api = axios.create({
        baseURL:'http://localhost:5000/',
        timeout: 10000
    })
    const[recipes, setRecipes] = useState([]);

    useEffect(()=>{
        getRecipes();
        if(props.connection !== undefined){
            console.log(props.connection)
            props.connection.on("NewRecipePosted", (recipes) =>{
                setRecipes(recipes);
            })
        }
    },[props.connection])

    useEffect(() => () => props.connection ? props.connection.off("NewRecipePosted") : null, []);

    function getRecipes(){
        api.get('recipe').then(res => {
            setRecipes(res.data);
            console.log(res.data);
        });
    }

    return <div>
        <Row sm={2} md={4} lg={6}>
            {recipes.map((recept) => (

                    <Col lg={2}>
                            <FoodCard text={recept.description} title={recept.name} id={recept.id} image={"https://localhost:5001/Uploads/" + recept.image}/>
                    </Col>

            ))}


        </Row>

    </div>
}

const mapStateToProps = (state) => {
    return {
        api: getAPI(state),
        connection: getConnection(state)
    };
};

export default connect(mapStateToProps)(FoodCardList);
