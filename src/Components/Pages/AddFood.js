import {Route} from "react-router";
import {Row, Col} from "react-grid-system";
import {TextField} from "@fluentui/react";
import * as React from "react";
import {useHistory} from "react-router-dom";
import {useState} from "react";
import {getAPI, getConnection} from "../../Core/Global/global.selectors";
import {connect} from "react-redux";

function AddFood(props){

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [ingredients, setIngredients] = useState("");
    const [preparation, setPreparation] = useState("");
    const [file, setFile] = useState(null);
    const [carbs, setCarbs] = useState("");

    const history = useHistory();
    const routeChange = () =>{
        let path = `Food`;
        history.push(path);
    }

    async function postedRecipe(){
        if(props.connection !== undefined){
            await props.connection.invoke("RecipePosted");
        }
    }
     function submitFood(){
        const formData = new FormData();
        formData.append("image", file);
        formData.append("name", name)
        formData.append("description", description)
        formData.append("ingredients", ingredients)
        formData.append("preparation", preparation)
        formData.append("carbs", carbs)
         if(props.api !== undefined){
             props.api.post('/recipe', formData, {
                 headers: {"Content-Type": "multipart/form-data"}
             }).then(() =>
                 postedRecipe()
             );
             routeChange();
         }

    }

    const onUpload = e =>{
        setFile(e.target.files[0]);
    }

    return(
            <Row>
                <Col sm={12} md={4} lg={4}>

                </Col>

                <Col sm={12} md={4} lg={4}>
                    <h2>Voeg je eigen recept toe!</h2>
                    <TextField label={"Naam recept"}
                               onChange={(e,value) => {
                                   setName(value);
                    }}  />
                    <TextField multiline rows={3}
                               label={"Omschrijving"}
                               onChange={(e,value) => {
                                   setDescription(value);
                               }}  />
                    <TextField multiline rows={3}
                               label={"Ingrediënten"}
                               onChange={(e,value) => {
                                    setIngredients(value);
                    }}  />
                    <TextField multiline rows={3}
                               label={"Voorbereiding"}
                               onChange={(e,value) => {
                                   setPreparation(value);
                    }}  />
                    <TextField label={"Koolhydraten"}
                               onChange={(e,value) => {
                                   setCarbs(value);
                               }}  />
                    <input type={"file"} name={"file"} onChange={onUpload}/>
                    <button className={"foodButton"}
                            style={{marginTop: "10px"}} onClick={submitFood}>Toevoegen</button>
                </Col>

                <Col sm={12} md={4} lg={4}>

                </Col>
            </Row>
    );



}
const mapStateToProps = (state) => {
    return {
        api: getAPI(state),
        connection: getConnection(state)
    };
};

export default connect(mapStateToProps)(AddFood);

