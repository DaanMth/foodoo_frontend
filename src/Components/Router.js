import Home from "./Pages/Home";
import Food from "./Pages/Food/Food";
import AddFood from "./Pages/AddFood"
import TextFields from "./Pages/Authentication/Login/Components/TextFields"
import {Route} from 'react-router-dom';
import {Col, Row} from "react-grid-system";
import Recipe from "./Pages/Food/Recipe/Recipe";
import Register from "./Pages/Authentication/Register"
import Login from "./Pages/Authentication/Login"

function Router(props){

    return(

        <div>
            <Route path={"/Home"} component={Home}/>
            <Row>
                <Col sm={12} md={2} lg={1}/>

                <Col sm={12} md={8} lg={10}>
                            <Route path={"/Food"} component={Food}/>
                            <Route exact path={"/Register"} component={Register}/>
                            <Route exact path={"/Login"} component={Login}/>
                            <Route path={"/addfood"} component={AddFood}/>
                            <Route path={"/recipe/:id"} render={(props) =>
                                <Recipe id={props.match.params.id}/>
                            }/>
                </Col>

                <Col sm={12} md={2} lg={1}/>
            </Row>

        </div>
    );
}
export default Router