import './App.css';
import Header from "./Components/Header/Header";
import {PrimaryButton, ThemeProvider, Toggle} from "@fluentui/react";
import {darkTheme, lightTheme} from "./Themes.js"
import Router from "./Components/Router";
import {getConnection, getTheme} from "./Core/Global/global.selectors";
import {connect} from "react-redux";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import {useEffect} from "react";
import {setAPI, setConnection} from "./Core/Global/global.actions";
import {HttpTransportType, HubConnectionBuilder, LogLevel} from "@microsoft/signalr";

function App(props) {
    const axios = require('axios');
    const api = axios.create({
        baseURL: 'http://localhost:5000/',
        timeout: 10000
    })
    async function startAndDispatch(connection){
        try{
            await connection.start();
            console.log("Connstarted")
            props.dispatch(setConnection(connection));
        } catch {
            console.log("Connection Failed");
        }
    }
    useEffect(() => {
        if (props.connection === undefined) {
            let connection = new HubConnectionBuilder().withUrl("https://localhost:5001/hub", {
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets
            }).configureLogging(LogLevel.Information).withAutomaticReconnect().build();
            startAndDispatch(connection);
        }
        props.dispatch(setAPI(api));
    }, [api])

    return <div style={{overflowX: "hidden"}}>
        <ThemeProvider applyTo={'body'} theme={props.theme ? darkTheme : lightTheme}>
            <Header/>
            <Router/>
        </ThemeProvider>
    </div>
}


const mapStateToProps = (state) => {
    return {
        theme: getTheme(state),
        connection: getConnection(state)
    };
};

export default connect(mapStateToProps)(App);
