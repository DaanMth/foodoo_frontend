export const Actions = {
    setTheme: "[Global] Theme",
    setAPI: "[Global] API",
    setConnection: "[Global] Connection"
}

export const setAPI = (api) => ({
    type: Actions.setAPI,
    payload:{
        api
    }
})
export const setTheme = (theme) => ({
    type: Actions.setTheme,
    payload: {
        theme
    }
})
export const setConnection = (connection) => ({
    type:Actions.setConnection,
    payload:{
        connection
    }
})