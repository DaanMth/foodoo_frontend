export const getGlobalState = store => store.global;

export const getTheme = store => getGlobalState(store).theme;
export const getAPI = store => getGlobalState(store).api;
export const getConnection = store => getGlobalState(store).connection;