import {Actions} from "./global.actions";

const initialState = {
    theme: true,
    api: undefined,
    connection: undefined
}

export default function globalReducer(state = initialState, action) {
    switch (action.type) {
        case Actions.setTheme:
            return {...state, theme: action.payload.theme}
        case Actions.setAPI:
            return {...state, api: action.payload.api}
        case Actions.setConnection:
            return {...state, connection: action.payload.connection}
        default:
            return state;
    }
}
